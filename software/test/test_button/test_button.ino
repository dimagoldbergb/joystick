int currentButtonStates[5] = {0,0,0,0,0};
int lastButtonStates[5] = {0,0,0,0,0};

void setup() {
  Serial.begin(9600);
  while(!Serial) {
    ;
  }
  // switches (cross)
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);
}

void loop() {

  // Read Switches
  updateButton(0, 2);
  updateButton(1, 3);
  updateButton(2, 4);
  updateButton(3, 5);

} 

void updateButton(int buttonNumber, int buttonPin) {
  currentButtonStates[buttonNumber] = digitalRead(buttonPin);
  if (currentButtonStates[buttonNumber] != lastButtonStates[buttonNumber]) {
    Serial.println(currentButtonStates[buttonNumber]);
    delay(1);
    lastButtonStates[buttonNumber] = currentButtonStates[buttonNumber];
  } 
}
